------------------------------------------------------------------------------
--                                                                          --
-- Script       :   createMisterProcesador.sql                              --
--                                                                          --
-- Autores      :   Christopher Flores  (10-10824),                         --
--                  Paul Baptista       (10-10056)                          --
--                                                                          --
--                                                                          --
-- Script de borrado de las tablas correspondientes al modelo relacional    --
-- diseñado para la base de datos de MisterProcesador.                      --
--                                                                          --
------------------------------------------------------------------------------


-- Se elimina la tabla ARBITRA.
DROP TABLE ARBITRA;

-- Se elimina la tabla AMONESTACION.
DROP TABLE AMONESTACION;

-- Se elimina la tabla JUEGA.
DROP TABLE JUEGA;

-- Se elimina la tabla CONTRATA.
DROP TABLE CONTRATA;

-- Se elimina la tabla ENTRENA.
DROP TABLE ENTRENA;

-- Se elimina la tabla GOL.
DROP TABLE GOL;

-- Se elimina la tabla JUGADOR.
DROP TABLE JUGADOR;

-- Se elimina la tabla ENTRENADOR.
DROP TABLE ENTRENADOR;

-- Se elimina la tabla ARBITRO.
DROP TABLE ARBITRO;

-- Se elimina la tabla PERSONA.
DROP TABLE PERSONA;

-- Se elimina la tabla PARTIDO.
DROP TABLE PARTIDO;

-- Se elimina la tabla EQUIPO.
DROP TABLE EQUIPO;

-- Se elimina la tabla ETAPA_O_JORNADA.
DROP TABLE ETAPA_O_JORNADA;

-- Se elimina la tabla COMPETENCIA.
DROP TABLE COMPETENCIA;
