INSERT INTO COMPETENCIA VALUES ('CHAMPIONS LEAGUE', 'UEFA', '2014',
                                'EUROPEAN CUP', 'COPA');

INSERT INTO COMPETENCIA VALUES ('FIFA WORLD CUP', 'FIFA', '2014',
                                'FIFA WORLD CUP TROPHY', 'COPA');

INSERT INTO COMPETENCIA VALUES ('LIGA ESPAÑOLA', 'LFP', '2014',
                                'CUALQUIERVERGA', 'LIGA', 'ESPAÑA');

INSERT INTO ETAPA_O_JORNADA VALUES ('FIFA WORLD CUP', 'FIFA', '2014',
                                    '10-10-2014', '20-12-2014');

INSERT INTO ETAPA_O_JORNADA VALUES ('CHAMPIONS LEAGUE', 'UEFA', '2014',
                                    '10-10-2014', '20-12-2014');

INSERT INTO EQUIPO VALUES ('Real Madrid', 'Madrid', 'España', 1900, 'pepe', 20000 );

INSERT INTO EQUIPO VALUES ('Barcelona FC', 'Madrid', 'España', 1900, 'Camp Nou', 20000 );

INSERT INTO PARTIDO VALUES ('Barcelona FC', 'Madrid', 'España', 'Real Madrid',
                            'Madrid', 'España', '15-10-2014',
                            'CHAMPIONS LEAGUE', 'UEFA', '2014', 20000, 5, 2, 1);

