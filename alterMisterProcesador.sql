------------------------------------------------------------------------------
--                                                                          --
-- Script       :   createMisterProcesador.sql                              --
--                                                                          --
-- Autores      :   Christopher Flores  (10-10824),                         --
--                  Paul Baptista       (10-10056)                          --
--                                                                          --
--                                                                          --
-- Script de agregado de restricciones a las tablas del modelo relacional   --
-- diseñado para la base de datos de MisterProcesador.                      --
--                                                                          --
------------------------------------------------------------------------------


--------------------------------------------------------------------
--    Restricciones de Dominio de los atributos de COMPETENCIA    --
--------------------------------------------------------------------

-- Verifica que el ANIO de la COMPETENCIA sea mayor que 1600.
ALTER TABLE COMPETENCIA ADD CONSTRAINT
    DOM_COMPETENCIA_ANIO CHECK (ANIO>1600);

-- Verifica que el TIPO de la COMPETENCIA sea 'LIGA' o 'COPA'.
ALTER TABLE COMPETENCIA ADD CONSTRAINT
    DOM_COMPETENCIA_TIPO CHECK (TIPO IN ('LIGA','COPA'));

-- Verifica que si la COMPETENCIA es de TIPO LIGA, el atributo PAIS no es NULL y
-- viceversa.
ALTER TABLE COMPETENCIA ADD CONSTRAINT
    COMPETENCIA_COPA_O_LIGA CHECK
        ( ( (TIPO='COPA') AND (PAIS is NULL) )
            OR ( (TIPO='LIGA') AND (PAIS is NOT NULL) ) );


------------------------------------------------------------------------
--    Restricciones de Dominio de los atributos de ETAPA_O_JORNADA    --
------------------------------------------------------------------------

-- Verifica que la FECHA_INICIO de la ETAPA_O_JORNADA sea menor a la FECHA_FIN.
ALTER TABLE ETAPA_O_JORNADA ADD CONSTRAINT
    CONS_ETAPA_O_JORNADA_FECHAS CHECK (FECHA_INICIO < FECHA_FIN);


---------------------------------------------------------------
--    Restricciones de Dominio de los atributos de EQUIPO    --
---------------------------------------------------------------

-- Verifica que el ANIO_FUNDACION del EQUIPO sea mayor a 1600.
ALTER TABLE EQUIPO ADD CONSTRAINT
    DOM_EQUIPO_ANIO_FUNDACION CHECK (ANIO_FUNDACION>1600);

-- Verifica que la CAPACIDAD_ESTADIO del EQUIPO sea positiva.
ALTER TABLE EQUIPO ADD CONSTRAINT
    DOM_EQUIPO_CAPACIDAD_ESTADIO CHECK (CAPACIDAD_ESTADIO>0);


----------------------------------------------------------------
--    Restricciones de Dominio de los atributos de PARTIDO    --
----------------------------------------------------------------

-- Verifica que la ASISTENTE al PARTIDO sea no negativa.
ALTER TABLE PARTIDO ADD CONSTRAINT 
    DOM_PARTIDO_ASISTENCIA_PUBLICO CHECK (ASISTENCIA_PUBLICO >= 0);
    
-- Verifica que la cantidad de MINUTOS_EXTRA del PARTIDO sea no negativa.
ALTER TABLE PARTIDO ADD CONSTRAINT
    DOM_PARTIDO_MINUTOS_EXTRA CHECK (MINUTOS_EXTRA >= 0);

-- Verifica que el RESULTADO_LOCAL sea no negativo.
ALTER TABLE PARTIDO ADD CONSTRAINT
    DOM_PARTIDO_RESULTADO_LOCAL CHECK (RESULTADO_LOCAL >= 0);

-- Verifica que el RESULTADO_VISIT sea no negativo.
ALTER TABLE PARTIDO ADD CONSTRAINT
    DOMPARTIDO_RESULTADO_VISIT CHECK (RESULTADO_VISIT >= 0);

-- Verifica que los PARTIDO que participan como LOCAL o VISITANTE sean
-- distintos.
ALTER TABLE PARTIDO ADD CONSTRAINT
    PARTIDO_LOCAL_DIF_VISIT CHECK
        ( (LOCAL_NOMBRE != VISITANTE_NOMBRE)
            OR (LOCAL_CIUDAD != VISITANTE_CIUDAD)
            OR (LOCAL_PAIS != VISITANTE_PAIS) );


----------------------------------------------------------------
--    Restricciones de Dominio de los atributos de JUGADOR    --
----------------------------------------------------------------

-- Verifica que el JUGADOR sea de LATERALIDAD DERECHO o ZURDO.
ALTER TABLE JUGADOR ADD CONSTRAINT
    DOM_JUGADOR_LATERALIDAD CHECK
        (LATERALIDAD IN  ('DERECHO','ZURDO'));


------------------------------------------------------------
--    Restricciones de Dominio de los atributos de GOL    --
------------------------------------------------------------

-- Verifica que el GOL sea anotado en un MINUTO mayor a cero.
ALTER TABLE GOL ADD CONSTRAINT
    DOM_GOL_MINUTO CHECK (MINUTO > 0);

-- Verifica que el TIPO del GOL anotado es de PIERNA_DERECHA, PIERNA_IZQDA,
-- CABEZA, PENALTI, TIRO_LIBREo PROPIA_PUERTA.
ALTER TABLE GOL ADD CONSTRAINT
    DOM_GOL_TIPO CHECK (TIPO IN 
        ('PIERNA_DERECHA','PIERNA_IZQDA','CABEZA','PENALTI','TIRO_LIBRE',
            'PROPIA_PUERTA') );

-- Verifica que en caso de que exista ASISTE_PASAPORTE (JUGADOR que asiste el
-- gol anotado), este sea distinto a ANOTA_PASAPORTE (JUGADOR anotador).
ALTER TABLE GOL ADD CONSTRAINT
    GOL_ASISTE_NO_ANOTA CHECK 
        ( (ASISTE_PASAPORTE IS NULL)
        OR ( (ASISTE_PASAPORTE IS NOT NULL)
            AND (ASISTE_PASAPORTE != ANOTA_PASAPORTE) ) );


-----------------------------------------------------------------
--    Restricciones de Dominio de los atributos de CONTRATA    --
-----------------------------------------------------------------

-- Verifica que el SALARIO_ANUAL tome un valor positivo.
ALTER TABLE CONTRATA ADD CONSTRAINT
    DOM_CONTRATA_SALARIO_ANUAL CHECK (SALARIO_ANUAL > 0);

-- Verifica que la CLAUSULA_RECESION tome un valor positivo.
ALTER TABLE CONTRATA ADD CONSTRAINT
    DOM_CONTRATA_CLAUSULA_RECESION CHECK (CLAUSULA_RECESION > 0);


--------------------------------------------------------------
--    Restricciones de Dominio de los atributos de JUEGA    --
--------------------------------------------------------------

-- Verifica que el MIN_INICIO en que el JUGADOR comienza a jugar tome un valor
-- positivo.
ALTER TABLE JUEGA ADD CONSTRAINT
    DOM_JUEGA_MIN_INICIO CHECK (MIN_INICIO > 0);
   
--  Verifica que el MIN_FINAL en que el JUGADOR termina de jugar tome un valor
-- positivo.
ALTER TABLE JUEGA ADD CONSTRAINT
    DOM_JUEGA_MIN_FINAL CHECK (MIN_FINAL > 0);
    
-- Verifica que el JUGADOR juegue con POSICION de PORTERO, DEFENSOR, 
-- MEDIOCAMPISTA o DELANTERO.
ALTER TABLE JUEGA ADD CONSTRAINT
    DOM_JUEGA_POSICION CHECK (POSICION IN
        ('PORTERO','DEFENSOR','MEDIOCAMPISTA','DELANTERO') );


---------------------------------------------------------------------
--    Restricciones de Dominio de los atributos de AMONESTACION    --
---------------------------------------------------------------------

-- Verifica que el TIPO de AMONESTACION sea AMARILLA o ROJA.
ALTER TABLE AMONESTACION ADD CONSTRAINT
    DOM_AMONESTACION_TIPO CHECK ( TIPO IN
        ('AMARILLA','ROJA') );

-- Verifica que el MINUTO en que se adjudica la amonestación tome un valor
-- positivo.
ALTER TABLE AMONESTACION ADD CONSTRAINT
    DOM_AMONESTACION_MINUTO CHECK (MINUTO > 0);


----------------------------------------------------------------
--    Restricciones de Dominio de los atributos de ARBITRA    --
----------------------------------------------------------------

-- Verifica que el ARBITRO que ARBITRA sea de tipo PRINCIPAL o ASISTENTE.
ALTER TABLE ARBITRA ADD CONSTRAINT
    DOM_ARBITRA_ARBITRO_TIPO CHECK (ARBITRO_TIPO IN
        ('PRINCIPAL','ASISTENTE') );