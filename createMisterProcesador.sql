------------------------------------------------------------------------------
--                                                                          --
-- Script       :   createMisterProcesador.sql                              --
--                                                                          --
-- Autores      :   Christopher Flores  (10-10824),                         --
--                  Paul Baptista       (10-10056)                          --
--                                                                          --
--                                                                          --
-- Script de creación de las tablas correspondientes al modelo relacional   --
-- diseñado para la base de datos de MisterProcesador.                      --
--                                                                          --
------------------------------------------------------------------------------


---------------------
--    ENTIDADES    --
---------------------


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación COMPETENCIA.
--
--  NOMBRE, NOMBRE_INSTITUCION y ANIO son la clave primaria de la misma.
CREATE TABLE COMPETENCIA(
    NOMBRE                      VARCHAR(40),
    NOMBRE_INSTITUCION          VARCHAR(40),
    ANIO                        NUMERIC(4),
    NOMBRE_TROFEO               VARCHAR(40) NOT NULL,
    TIPO                        VARCHAR(4)  NOT NULL,
    PAIS                        VARCHAR(40),

    CONSTRAINT PK_COMPETENCIA 
        PRIMARY KEY (NOMBRE,NOMBRE_INSTITUCION,ANIO)
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación ETAPA_O_JORNADA.
--
--  COMP_NOMBRE, COMP_NOMBRE_INSTITUCION y COMP_ANIO son clave foranea, que
-- hace referencia a COMPETENCIA.
--
--  COMP_NOMBRE, COMP_NOMBRE_INSTITUCION, COMP_ANIO y FECHA_INICIO son la clave
-- primaria.
--------------------------------------------------------------------------------
CREATE TABLE ETAPA_O_JORNADA(
    COMP_NOMBRE                 VARCHAR(40),
    COMP_NOMBRE_INSTITUCION     VARCHAR(40),
    COMP_ANIO                   NUMERIC(4),

    FECHA_INICIO                TIMESTAMP,
    FECHA_FIN                   TIMESTAMP   NOT NULL,

    CONSTRAINT PK_ETAPA_O_JORNADA
        PRIMARY KEY (COMP_NOMBRE, COMP_NOMBRE_INSTITUCION, COMP_ANIO, 
                    FECHA_INICIO),

    CONSTRAINT JK_ETAPA_O_JORNADA
        FOREIGN KEY (COMP_NOMBRE, COMP_NOMBRE_INSTITUCION, COMP_ANIO)
            REFERENCES COMPETENCIA
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación EQUIPO.
--
--  NOMBRE, CIUDAD y PAIS son la clave primaria de la misma.
--------------------------------------------------------------------------------
CREATE TABLE EQUIPO(
    NOMBRE                      VARCHAR(40),
    CIUDAD                      VARCHAR(40),
    PAIS                        VARCHAR(40),
    ANIO_FUNDACION              NUMERIC(4)  NOT NULL,
    NOMBRE_ESTADIO              VARCHAR(40) NOT NULL,
    CAPACIDAD_ESTADIO           INTEGER     NOT NULL,
    
    CONSTRAINT PK_EQUIPO
        PRIMARY KEY (NOMBRE, CIUDAD, PAIS)
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación PARTIDO.
--
--  LOCAL_NOMBRE, LOCAL_CIUDAD y LOCAL_PAIS son clave foranea, hace referencia a
-- EQUIPO.
--
--  VISITANTE_NOMBRE, VISITANTE_CIUDAD y VISITANTE_PAIS son clave foranea misma,
-- hace otra referencia a EQUIPO.
--
--  LOCAL_NOMBRE, LOCAL_CIUDAD, LOCAL_PAIS, VISITANTE_NOMBRE, VISITANTE_CIUDAD,
-- VISITANTE_PAIS y FECHA son la clave primaria.
--------------------------------------------------------------------------------
CREATE TABLE PARTIDO(
    LOCAL_NOMBRE                VARCHAR(40),
    LOCAL_CIUDAD                VARCHAR(40),
    LOCAL_PAIS                  VARCHAR(40),
    VISITANTE_NOMBRE            VARCHAR(40),
    VISITANTE_CIUDAD            VARCHAR(40),
    VISITANTE_PAIS              VARCHAR(40),
    FECHA                       TIMESTAMP,

    COMP_NOMBRE                 VARCHAR(40) NOT NULL,
    COMP_NOMBRE_INSTITUCION     VARCHAR(40) NOT NULL,
    COMP_ANIO                   NUMERIC(4)  NOT NULL,
    
    ASISTENCIA_PUBLICO          INTEGER     NOT NULL,
    MINUTOS_EXTRA               INTEGER     NOT NULL,
    RESULTADO_LOCAL             INTEGER     NOT NULL,
    RESULTADO_VISIT             INTEGER     NOT NULL,

    CONSTRAINT PK_PARTIO
        PRIMARY KEY 
            (LOCAL_NOMBRE, LOCAL_CIUDAD, LOCAL_PAIS, VISITANTE_NOMBRE, 
            VISITANTE_CIUDAD, VISITANTE_PAIS, FECHA),

    CONSTRAINT FK_PARTIDO_LOCAL
        FOREIGN KEY (LOCAL_NOMBRE, LOCAL_CIUDAD, LOCAL_PAIS)
            REFERENCES EQUIPO,

    CONSTRAINT FK_PARTIDO_VISIT
        FOREIGN KEY (VISITANTE_NOMBRE, VISITANTE_CIUDAD, VISITANTE_PAIS)
            REFERENCES EQUIPO
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación PERSONA.
--
--  PASAPORTE es la clave primaria.
--------------------------------------------------------------------------------
CREATE TABLE PERSONA(
    PASAPORTE                   VARCHAR(9),
    NOMBRE                      VARCHAR(40) NOT NULL,
    NACIONALIDAD                VARCHAR(40) NOT NULL,

    CONSTRAINT PK_PERSONA
        PRIMARY KEY (PASAPORTE)
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación ARBITRO.
--
--  PASAPORTE es clave foranea, hace referencia a PERSONA.
--
--  PASAPORTE es la clave primaria.
--------------------------------------------------------------------------------
CREATE TABLE ARBITRO(
    PASAPORTE                   VARCHAR(9),

    CONSTRAINT PK_ARBITRO
        PRIMARY KEY (PASAPORTE),

    CONSTRAINT FK_ARBITRO
        FOREIGN KEY (PASAPORTE)
            REFERENCES PERSONA
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación ENTRENADOR.
--
--  PASAPORTE es clave foranea, hace referencia a PERSONA.
--
--  PASAPORTE es la clave primaria.
--------------------------------------------------------------------------------
CREATE TABLE ENTRENADOR(
    PASAPORTE                   VARCHAR(9),

    CONSTRAINT PK_ENTRENADOR
        PRIMARY KEY (PASAPORTE),

    CONSTRAINT FK_ENTRENADOR
        FOREIGN KEY (PASAPORTE)
            REFERENCES PERSONA
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación JUGADOR.
--
--  PASAPORTE es clave foranea, hace referencia a PERSONA.
--
--  PASAPORTE es la clave primaria.
--------------------------------------------------------------------------------
CREATE TABLE JUGADOR(
    PASAPORTE                   VARCHAR(9),
    PESO                        VARCHAR(40) NOT NULL,
    ESTATURA                    VARCHAR(40) NOT NULL,
    LATERALIDAD                 VARCHAR(40) NOT NULL,
    FECHA_NACIMIENTO            VARCHAR(40) NOT NULL,

    CONSTRAINT PK_JUGADOR
        PRIMARY KEY (PASAPORTE),

    CONSTRAINT FK_JUGADOR
        FOREIGN KEY (PASAPORTE)
            REFERENCES PERSONA
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación GOL.
--
--  ANOTA_PASAPORTE es clave foranea, hace referencia a PERSONA.
--
--  ASISTE_PASAPORTE es clave foranea, hace otra referencia a PERSONA.
--
--  PARTIDO_LOCAL_NOMBRE, PARTIDO_LOCAL_CIUDAD, PARTIDO_LOCAL_PAIS,
-- PARTIDO_VISITANTE_NOMBRE, PARTIDO_VISITANTE_CIUDAD, PARTIDO_VISITANTE_PAIS y
-- PARTIDO_FECHA son clave foranea, que referencia a PARTIDO.
--
--  ANOTA_PASAPORTE, PARTIDO_LOCAL_NOMBRE, PARTIDO_LOCAL_CIUDAD,
-- PARTIDO_LOCAL_PAIS, PARTIDO_VISITANTE_NOMBRE, PARTIDO_VISITANTE_CIUDAD,
-- PARTIDO_VISITANTE_PAIS, PARTIDO_FECHA y MINUTO son la clave primaria.
--------------------------------------------------------------------------------
CREATE TABLE GOL(
    ANOTA_PASAPORTE             VARCHAR(9),

    PARTIDO_LOCAL_NOMBRE        VARCHAR(40),
    PARTIDO_LOCAL_CIUDAD        VARCHAR(40),
    PARTIDO_LOCAL_PAIS          VARCHAR(40),
    PARTIDO_VISITANTE_NOMBRE    VARCHAR(40),
    PARTIDO_VISITANTE_CIUDAD    VARCHAR(40),
    PARTIDO_VISITANTE_PAIS      VARCHAR(40),
    PARTIDO_FECHA               TIMESTAMP,

    ASISTE_PASAPORTE            VARCHAR(9),
    MINUTO                      INTEGER,
    TIPO                        VARCHAR(40) NOT NULL,

    CONSTRAINT PK_GOL
        PRIMARY KEY (ANOTA_PASAPORTE, PARTIDO_LOCAL_NOMBRE,
                    PARTIDO_LOCAL_CIUDAD, PARTIDO_LOCAL_PAIS,
                    PARTIDO_VISITANTE_NOMBRE, PARTIDO_VISITANTE_CIUDAD,
                    PARTIDO_VISITANTE_PAIS, PARTIDO_FECHA, MINUTO),

    CONSTRAINT FK_GOL_ANOTA
        FOREIGN KEY (ANOTA_PASAPORTE)
            REFERENCES JUGADOR,

    CONSTRAINT FK_GOL_ASISTE
        FOREIGN KEY (ASISTE_PASAPORTE)
            REFERENCES JUGADOR,

    CONSTRAINT FK_GOL_JUEGO
        FOREIGN KEY (PARTIDO_LOCAL_NOMBRE, PARTIDO_LOCAL_CIUDAD,
                    PARTIDO_LOCAL_PAIS, PARTIDO_VISITANTE_NOMBRE,
                    PARTIDO_VISITANTE_CIUDAD, PARTIDO_VISITANTE_PAIS, PARTIDO_FECHA)
            REFERENCES PARTIDO
);



---------------------------
--    INTERRELACIONES    --
---------------------------


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación ENTRENA.
--
--  PASAPORTE es clave foranea, hace referencia a ENTRENADOR.
--
--  NOMBRE_EQUIPO, CIUDAD_EQUIPO y PAIS_EQUIPO son clave foranea, hace
-- referencia a EQUIPO.
--
--  PASAPORTE, NOMBRE_EQUIPO, CIUDAD_EQUIPO, FECHA_NOMBRAMIENTO son la clave
-- primaria.
--------------------------------------------------------------------------------
CREATE TABLE ENTRENA(
    PASAPORTE                   VARCHAR(9),
    NOMBRE_EQUIPO               VARCHAR(40),
    CIUDAD_EQUIPO               VARCHAR(40),
    PAIS_EQUIPO                 VARCHAR(40),
    FECHA_NOMBRAMIENTO          TIMESTAMP,
    FECHA_FINALIZACION          TIMESTAMP   NOT NULL,

    CONSTRAINT PK_ENTRENA
        PRIMARY KEY (PASAPORTE, NOMBRE_EQUIPO, CIUDAD_EQUIPO,
                    FECHA_NOMBRAMIENTO),

    CONSTRAINT FK_ENTRENTRENADOR
        FOREIGN KEY(PASAPORTE)
            REFERENCES ENTRENADOR,

    CONSTRAINT FK_ENTREQUIPO
        FOREIGN KEY(NOMBRE_EQUIPO, CIUDAD_EQUIPO, PAIS_EQUIPO)
            REFERENCES EQUIPO
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación CONTRATA.
--
--  PASAPORTE es clave foranea, hace referencia a JUGADOR.
--
--  NOMBRE, CIUDAD, PAIS son clave foranea, hace referencia a EQUIPO.
-- 
--  NOMBRE, CIUDAD, PAIS, PASAPORTE y FECHA_INICIO es la clave primaria.
--------------------------------------------------------------------------------
CREATE TABLE CONTRATA(
    NOMBRE                      VARCHAR(40),
    CIUDAD                      VARCHAR(40),
    PAIS                        VARCHAR(40),
    PASAPORTE                   VARCHAR(9),
    FECHA_INICIO                TIMESTAMP,
    FECHA_FINALIZACION          TIMESTAMP   NOT NULL,
    SALARIO_ANUAL               INTEGER     NOT NULL,
    CLAUSULA_RECESION           INTEGER     NOT NULL,

    CONSTRAINT PK_CONTRATA
        PRIMARY KEY (NOMBRE, CIUDAD, PAIS, PASAPORTE, FECHA_INICIO),
    
    CONSTRAINT FK_CONTRATA_EQUIPO
        FOREIGN KEY(NOMBRE, CIUDAD, CIUDAD)
            REFERENCES EQUIPO,
    
    CONSTRAINT FK_CONTRATA_JUGADOR
        FOREIGN KEY(PASAPORTE)
            REFERENCES JUGADOR
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación JUEGA.
--
--  JUGADOR_PASAPORTE es clave foranea, hace referencia a JUGADOR.
--
--  PARTIDO_LOCAL_NOMBRE, PARTIDO_LOCAL_CIUDAD, PARTIDO_LOCAL_PAIS,
-- PARTIDO_VISITANTE_NOMBRE, PARTIDO_VISITANTE_CIUDAD, PARTIDO_VISITANTE_PAIS y
-- PARTIDO_FECHA son clave foranea, hace referencia a PARTIDO.
-- 
--  JUGADOR_PASAPORTE, PARTIDO_LOCAL_NOMBRE, PARTIDO_LOCAL_CIUDAD, 
-- PARTIDO_LOCAL_PAIS, PARTIDO_VISITANTE_NOMBRE, PARTIDO_VISITANTE_CIUDAD,
-- PARTIDO_VISITANTE_PAIS y PARTIDO_FECHA, son la clave primaria.
--------------------------------------------------------------------------------
CREATE TABLE JUEGA (

    JUGADOR_PASAPORTE           VARCHAR(9),
    PARTIDO_LOCAL_NOMBRE        VARCHAR(40),
    PARTIDO_LOCAL_CIUDAD        VARCHAR(40),
    PARTIDO_LOCAL_PAIS          VARCHAR(40),
    PARTIDO_VISITANTE_NOMBRE    VARCHAR(40),
    PARTIDO_VISITANTE_CIUDAD    VARCHAR(40),
    PARTIDO_VISITANTE_PAIS      VARCHAR(40),
    PARTIDO_FECHA               TIMESTAMP,
    MIN_INICIO                  INTEGER     NOT NULL,
    MIN_FINAL                   INTEGER     NOT NULL,
    POSICION                    VARCHAR(40) NOT NULL,

    CONSTRAINT PK_JUEGA
        PRIMARY KEY (JUGADOR_PASAPORTE, PARTIDO_LOCAL_NOMBRE,
                    PARTIDO_LOCAL_CIUDAD, PARTIDO_LOCAL_PAIS,
                    PARTIDO_VISITANTE_NOMBRE, PARTIDO_VISITANTE_CIUDAD,
                    PARTIDO_VISITANTE_PAIS, PARTIDO_FECHA ),
    
    CONSTRAINT FK_JUEGA_JUGADOR
        FOREIGN KEY (JUGADOR_PASAPORTE)
            REFERENCES JUGADOR,
    
    CONSTRAINT FK_JUEGA_PARTIDO
        FOREIGN KEY (PARTIDO_LOCAL_NOMBRE, PARTIDO_LOCAL_CIUDAD,
                    PARTIDO_LOCAL_PAIS, PARTIDO_VISITANTE_NOMBRE,
                    PARTIDO_VISITANTE_CIUDAD, PARTIDO_VISITANTE_PAIS,PARTIDO_FECHA )
            REFERENCES PARTIDO
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación AMONESTACION.
--
--  JUEGA_PASAPORTE, JUEGA_LOCAL_NOMBRE, JUEGA_LOCAL_CIUDAD, JUEGA_LOCAL_PAIS,
-- JUEGA_VISITANTE_NOMBRE, JUEGA_VISITANTE_CIUDAD, JUEGA_VISITANTE_PAIS y FECHA
-- son clave foranea, hace referencia a JUEGA.
-- 
--  JUEGA_PASAPORTE, JUEGA_LOCAL_NOMBRE, JUEGA_LOCAL_CIUDAD, JUEGA_LOCAL_PAIS,
-- JUEGA_VISITANTE_NOMBRE, JUEGA_VISITANTE_CIUDAD, JUEGA_VISITANTE_PAIS, FECHA,
-- TIPO y MINUTO son la clave primaria.
--------------------------------------------------------------------------------
CREATE TABLE AMONESTACION(

    JUEGA_PASAPORTE             VARCHAR(9),
    JUEGA_LOCAL_NOMBRE          VARCHAR(40),
    JUEGA_LOCAL_CIUDAD          VARCHAR(40),
    JUEGA_LOCAL_PAIS            VARCHAR(40),
    JUEGA_VISITANTE_NOMBRE      VARCHAR(40),
    JUEGA_VISITANTE_CIUDAD      VARCHAR(40),
    JUEGA_VISITANTE_PAIS        VARCHAR(40),
    FECHA                       TIMESTAMP     NOT NULL,
    TIPO                        VARCHAR(40)   NOT NULL,
    MINUTO                      INTEGER       NOT NULL,

    CONSTRAINT PK_AMONESTACION
        PRIMARY KEY (JUEGA_PASAPORTE, JUEGA_LOCAL_NOMBRE, JUEGA_LOCAL_CIUDAD,
                    JUEGA_LOCAL_PAIS, JUEGA_VISITANTE_NOMBRE,
                    JUEGA_VISITANTE_CIUDAD, JUEGA_VISITANTE_PAIS, FECHA, TIPO,
                    MINUTO),
    
    CONSTRAINT FK_AMONESTACION
        FOREIGN KEY (JUEGA_PASAPORTE, JUEGA_LOCAL_NOMBRE, JUEGA_LOCAL_CIUDAD,
                    JUEGA_LOCAL_PAIS, JUEGA_VISITANTE_NOMBRE,
                    JUEGA_VISITANTE_CIUDAD, JUEGA_VISITANTE_PAIS, FECHA)
        REFERENCES JUEGA
);


--------------------------------------------------------------------------------
--  Tabla en la que se representa la relación ARBITRA.
--
--  ARBITRO_PASAPORTE es clave foranea, hace referencia a ARBITRO.
--
--  PARTIDO_LOCAL_NOMBRE, PARTIDO_LOCAL_CIUDAD, PARTIDO_LOCAL_PAIS,
-- PARTIDO_VISITANTE_NOMBRE, PARTIDO_VISITANTE_CIUDAD, PARTIDO_VISITANTE_PAIS y
-- PARTIDO_FECHA son clave foranea, hace referencia a PARTIDO.
--
--  ARBITRO_PASAPORTE, PARTIDO_LOCAL_NOMBRE, PARTIDO_LOCAL_CIUDAD,
-- PARTIDO_LOCAL_PAIS, PARTIDO_VISITANTE_NOMBRE, PARTIDO_VISITANTE_CIUDAD,
-- PARTIDO_VISITANTE_PAISy PARTIDO_FECHA son la clave primaria.
--------------------------------------------------------------------------------
CREATE TABLE ARBITRA(
    ARBITRO_PASAPORTE           VARCHAR(9),
    PARTIDO_LOCAL_NOMBRE        VARCHAR(40),
    PARTIDO_LOCAL_CIUDAD        VARCHAR(40),
    PARTIDO_LOCAL_PAIS          VARCHAR(40),
    PARTIDO_VISITANTE_NOMBRE    VARCHAR(40),
    PARTIDO_VISITANTE_CIUDAD    VARCHAR(40),
    PARTIDO_VISITANTE_PAIS      VARCHAR(40),
    PARTIDO_FECHA               TIMESTAMP,
    ARBITRO_TIPO                VARCHAR(40) NOT NULL,

    CONSTRAINT PK_ARBITRA
        PRIMARY KEY (ARBITRO_PASAPORTE, PARTIDO_LOCAL_NOMBRE,
                    PARTIDO_LOCAL_CIUDAD, PARTIDO_LOCAL_PAIS,
                    PARTIDO_VISITANTE_NOMBRE, PARTIDO_VISITANTE_CIUDAD,
                    PARTIDO_VISITANTE_PAIS, PARTIDO_FECHA),

    CONSTRAINT FK_ARBITRA_ARBITRO
        FOREIGN KEY (ARBITRO_PASAPORTE)
            REFERENCES ARBITRO,

    CONSTRAINT FK_ARBITRA_PARTIDO
        FOREIGN KEY (PARTIDO_LOCAL_NOMBRE, PARTIDO_LOCAL_CIUDAD,
                    PARTIDO_LOCAL_PAIS, PARTIDO_VISITANTE_NOMBRE, 
                    PARTIDO_VISITANTE_CIUDAD, PARTIDO_VISITANTE_PAIS,
                    PARTIDO_FECHA)
            REFERENCES PARTIDO
);